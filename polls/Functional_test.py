__author__ = 'asistente'
from unittest import TestCase
from selenium import webdriver
from selenium.webdriver.common.by import By

class FunctionalTest(TestCase):

    def setUp(self):
        self.browser = webdriver.Chrome("C:/Users/Efrai/Documents/chromedriver_win32/chromedriver.exe")
        self.browser.implicitly_wait(2)

    def tearDown(self):
        self.browser.quit()

    def test_title(self):
        self.browser.get('http://localhost:8000')
        self.assertIn('Busco Ayuda', self.browser.title)

    def test_registro(self):
        self.browser.get('http://localhost:8000')
        link = self.browser.find_element_by_id('id_register')
        link.click()

        nombre = self.browser.find_element_by_id('id_nombre')
        nombre.send_keys('Cosme')

        apellidos = self.browser.find_element_by_id('id_apellidos')
        apellidos.send_keys('Fulanito')

        experiencia = self.browser.find_element_by_id('id_aniosExperiencia')
        experiencia.send_keys('3')

        self.browser.find_element_by_xpath("//select[@id='id_tiposDeServicio']/option[text()='Desarrollador Web']").click()

        telefono = self.browser.find_element_by_id('id_telefono')
        telefono.send_keys('3209624591')

        correo = self.browser.find_element_by_id('id_correo')
        correo.send_keys('e.aperador@uniandes.edu.co')

        imagen = self.browser.find_element_by_id('id_imagen')
        imagen.send_keys('C:/Users/Efrai/Documents/desarrollo-web.jpg')

        nombreUsuario = self.browser.find_element_by_id('id_username')
        nombreUsuario.send_keys('e.aperador')

        clave = self.browser.find_element_by_id('id_password')
        clave.send_keys('clave123')

        botonGrabar = self.browser.find_element_by_id('id_grabar')
        botonGrabar.click()
        self.browser.implicitly_wait(3)
        span=self.browser.find_element(By.XPATH, '//span[text()="Cosme Fulanito"]')

        self.assertIn('Cosme Fulanito', span.text)

    def test_detalle(self):
        self.browser.get('http://localhost:8000')
        linkDetalle = self.browser.find_element_by_css_selector('.trabajador > span')
        linkDetalle.click()
        self.browser.implicitly_wait(3)

        nombre = self.browser.find_element_by_css_selector('#nombres > h2').text
        self.assertIn(nombre, 'Cosme Fulanito')

    def test_login(self):
        self.browser.get('http://localhost:8000')
        linkLogin = self.browser.find_element_by_css_selector('#id_login')
        linkLogin.click()
        self.browser.implicitly_wait(3)

        usuario = self.browser.find_element_by_css_selector('#usrname')
        usuario.send_keys('e.aperador')

        password = self.browser.find_element_by_css_selector('#psw')
        password.send_keys('clave123')

        ingresar = self.browser.find_element_by_css_selector('#id_boton_login')
        ingresar.click()
        self.browser.implicitly_wait(3)

        alerta = self.browser.find_element_by_css_selector('.alert.alert-success > strong').text
        print(alerta)
        self.assertIn(alerta, 'SUCCESS: ')

    def test_editar(self):
        self.browser.get('http://localhost:8000')
        linkLogin = self.browser.find_element_by_css_selector('#id_login')
        linkLogin.click()
        self.browser.implicitly_wait(3)

        usuario = self.browser.find_element_by_css_selector('#usrname')
        usuario.send_keys('e.aperador')

        password = self.browser.find_element_by_css_selector('#psw')
        password.send_keys('clave123')

        ingresar = self.browser.find_element_by_css_selector('#id_boton_login')
        ingresar.click()
        self.browser.implicitly_wait(10)

        editar = self.browser.find_element_by_css_selector('#id_editar')
        editar.click()

        apellidos = self.browser.find_element_by_css_selector('#id_apellidos')
        apellidos.send_keys('Nuevo Apellido')

        grabar = self.browser.find_element_by_css_selector('#id_grabar')
        grabar.click()

        url = self.browser.current_url
        print(url)
        self.assertIn(url, 'http://localhost:8000/')